package com.vimash.vimashbe.Enum;

public enum ResponseStatus {
    SUCCESS("success"),
    FAILURE("failure"),
    ERROR("error");
    private String value;

    ResponseStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
