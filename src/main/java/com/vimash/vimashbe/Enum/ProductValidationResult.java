package com.vimash.vimashbe.Enum;

public enum ProductValidationResult {
    SUCCESS,
    CODE_INVALID,
    NAME_INVALID
}
