package com.vimash.vimashbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViMashBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViMashBeApplication.class, args);
    }

}
