package com.vimash.vimashbe.controller;

import com.vimash.vimashbe.dto.BaseResponse;
import com.vimash.vimashbe.dto.ProductSaveDto;
import com.vimash.vimashbe.entity.ProductEntity;
import com.vimash.vimashbe.service.ProductService;
import com.vimash.vimashbe.service.faker_data.FakeDataService;
import com.vimash.vimashbe.utils.GenerateBaseResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("*")
@RequestMapping("/product")
@RequiredArgsConstructor
public class PublicApiV1 {
    private final ProductService productService;
    private final FakeDataService fakeDataService;

    @PostMapping("")
    public ResponseEntity<BaseResponse> saveProduct(@RequestBody ProductSaveDto product) {
        return new ResponseEntity<>(productService.saveProduct(product), HttpStatus.OK);
    }

    @GetMapping("/fake_data")
    public ResponseEntity<?> fakeData() {
        this.fakeDataService.fakeProduct();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("")
    public ResponseEntity<BaseResponse> editProduct(@RequestBody ProductEntity product) {
        return new ResponseEntity<>(productService.editProduct(product), HttpStatus.OK);
    }

    @DeleteMapping("")
    public ResponseEntity<BaseResponse> deleteProduct(@RequestParam(name = "id") Integer id) {
        return new ResponseEntity<>(productService.deleteProduct(id), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<BaseResponse> getProduct(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "search", required = false, defaultValue = "") String search) {
        Pageable pageable = PageRequest.of(page, 12);

        Page<ProductEntity> entityPage = this.productService.getProducts(search, pageable);
        if (entityPage.getContent().isEmpty()) {
            if (page == 0) {
                return new ResponseEntity<>(GenerateBaseResponse.notFound("Not found product with Name or Code containing", entityPage), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(GenerateBaseResponse.noContentGetResponse("No Content", entityPage), HttpStatus.OK);
            }
        }
        if (search.isBlank()) {
            return new ResponseEntity<>(GenerateBaseResponse.successGetResponse("get list success", entityPage), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(GenerateBaseResponse.successGetResponse("get list with search success", entityPage), HttpStatus.OK);
        }
    }
}
