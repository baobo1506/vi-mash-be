package com.vimash.vimashbe.dto;

import lombok.*;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
public class ProductSaveDto {
    private String code;
    private String name;
}
