package com.vimash.vimashbe.dto;

import com.vimash.vimashbe.Enum.ResponseStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class BaseResponse {
    private int code;
    private String status;
    private String msg;
    private Date time;
    private List<Object> data;
}
