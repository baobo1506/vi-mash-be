package com.vimash.vimashbe.utils;


import com.vimash.vimashbe.Enum.ResponseStatus;
import com.vimash.vimashbe.dto.BaseResponse;
import org.springframework.http.HttpStatus;

import java.util.*;

public class GenerateBaseResponse {
    private GenerateBaseResponse() {
        throw new IllegalStateException("Utility class");
    }

    public static BaseResponse successGetResponse(String msg, Object data) {
        return BaseResponse.builder()
                .code(HttpStatus.OK.value())
                .msg(msg)
                .data(new ArrayList<>(Collections.singletonList(data)))
                .status(ResponseStatus.SUCCESS.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse successGetResponse(String msg, List<Object> data) {
        return BaseResponse.builder()
                .code(HttpStatus.OK.value())
                .msg(msg)
                .data(data)
                .status(ResponseStatus.SUCCESS.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse noContentGetResponse(String msg, Object data) {
        return BaseResponse.builder()
                .code(HttpStatus.NO_CONTENT.value())
                .msg(msg)
                .data(new ArrayList<Object>(Collections.singletonList(data)))
                .status(ResponseStatus.SUCCESS.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse noContentGetResponse(String msg, List<Object> data) {
        return BaseResponse.builder()
                .code(HttpStatus.NO_CONTENT.value())
                .msg(msg)
                .data(data)
                .status(ResponseStatus.SUCCESS.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse invalidField(String msg, Object data) {
        return BaseResponse.builder()
                .code(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .msg(msg)
                .data(new ArrayList<Object>(Collections.singletonList(data)))
                .status(ResponseStatus.FAILURE.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse invalidField(String msg, List<Object> data) {
        return BaseResponse.builder()
                .code(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .msg(msg)
                .data(data)
                .status(ResponseStatus.FAILURE.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse notFound(String msg, Object data) {
        return BaseResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .msg(msg)
                .data(new ArrayList<Object>(Collections.singletonList(data)))
                .status(ResponseStatus.FAILURE.getValue())
                .time(new Date())
                .build();
    }

    public static BaseResponse notFound(String msg, List<Object> data) {
        return BaseResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .msg(msg)
                .data(data)
                .status(ResponseStatus.FAILURE.getValue())
                .time(new Date())
                .build();
    }
}
