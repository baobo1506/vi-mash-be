package com.vimash.vimashbe.service;

import com.vimash.vimashbe.dto.BaseResponse;
import com.vimash.vimashbe.dto.ProductSaveDto;
import com.vimash.vimashbe.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ProductService {
    BaseResponse saveProduct(ProductSaveDto productEntity);

    Page<ProductEntity> getProducts(String search, Pageable pageable);

    BaseResponse editProduct(ProductEntity product);

    BaseResponse deleteProduct(Integer id);
}
