package com.vimash.vimashbe.service.faker_data.impl;

import com.vimash.vimashbe.entity.ProductEntity;
import com.vimash.vimashbe.repository.ProductRepository;
import com.vimash.vimashbe.service.faker_data.FakeDataService;
import lombok.RequiredArgsConstructor;
import net.datafaker.Faker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class FakeDataServiceImpl implements FakeDataService {
    private final Faker faker = new Faker();
    private final ProductRepository productRepository;


    @Override
    public void fakeProduct() {
        for (int i = 0; i < 100; i++) {
            ProductEntity productEntity = ProductEntity.builder()
                    .code(this.faker.expression("#{regexify '(CODE)[0-9]{3}'}"))
                    .name(this.faker.leagueOfLegends().champion())
                    .deleted(false)
                    .build();
            productRepository.save(productEntity);
        }
    }
}
