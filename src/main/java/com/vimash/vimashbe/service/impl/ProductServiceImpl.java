package com.vimash.vimashbe.service.impl;

import com.vimash.vimashbe.dto.BaseResponse;
import com.vimash.vimashbe.dto.ProductSaveDto;
import com.vimash.vimashbe.entity.ProductEntity;
import com.vimash.vimashbe.repository.ProductRepository;
import com.vimash.vimashbe.service.ProductService;
import com.vimash.vimashbe.utils.GenerateBaseResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public BaseResponse saveProduct(ProductSaveDto productDto) {
        ProductEntity productEntity = ProductEntity.builder()
                .code(productDto.getCode())
                .name(productDto.getName())
                .build();
        productEntity = productRepository.save(productEntity);
        return GenerateBaseResponse.successGetResponse("save succeed", productEntity.getId());
    }

    @Override
    public Page<ProductEntity> getProducts(String search, Pageable pageable) {
        return productRepository.searchByCodeContainingAndDeletedOrNameContainingAndDeleted(search, false, search, false, pageable);
    }

    @Override
    public BaseResponse editProduct(ProductEntity product) {
        // Validate here
        productRepository.save(product);
        return GenerateBaseResponse.successGetResponse("edit success", product.getId());
    }

    @Override
    public BaseResponse deleteProduct(Integer id) {
        // Validate here
        productRepository.deleteById(id);
        return GenerateBaseResponse.successGetResponse("delete success", id);
    }


}
