package com.vimash.vimashbe.repository;

import com.vimash.vimashbe.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {
    Page<ProductEntity> searchByCodeContainingAndDeletedOrNameContainingAndDeleted(String code, boolean delete1, String name, boolean delete2, Pageable pageable);

}
