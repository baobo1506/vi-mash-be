package com.vimash.vimashbe.validation;

import com.vimash.vimashbe.Enum.ProductValidationResult;
import com.vimash.vimashbe.entity.ProductEntity;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.vimash.vimashbe.Enum.ProductValidationResult.*;

public interface ProductValidator extends Function<ProductEntity, ProductValidationResult> {
    static ProductValidator isCodeValid() {
        return product -> product.getCode().matches("(CODE)\\d{3}") ?
                SUCCESS : CODE_INVALID;
    }

    static ProductValidator isNameValid() {
        return product -> product.getName().trim().length() > 3 && product.getName().trim().length() < 100 ?
                SUCCESS : NAME_INVALID;
    }

    default ProductValidator and(ProductValidator otherValidator, List<ProductValidationResult> validationResultList) {
        return product -> {
            ProductValidationResult result = this.apply(product);
            validationResultList.add(result);
            return SUCCESS;
        };
    }
}
